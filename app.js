const express = require('express');
const app = express();
const port = "8000";
const generatePassword = require('generate-password')
const axios = require('axios');
const { response } = require('express');
const bcrypt = require('bcrypt')

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get('/', (req, res) =>{
    res.sendFile (__dirname + "/index.html")
})

app.post('/', (req, res) => {
    const userEmail = req.body.email
    const userPassword = req.body.password
    const url = "https://reqres.in/api/login"
    try {
        axios({
            method: 'get',
            url: url,
            responseType: 'json'
        })
        .then(function (resp) {
            const name = resp.data[0].name
            console.log(name);
        })
        
    } catch (error) {
        console.log(error);
    }
})

app.listen(port, () => {
    console.log(`Example app is running on ${port}`);
})